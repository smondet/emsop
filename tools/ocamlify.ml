
let variable_name = Sys.argv.(1)
let input_file = Sys.argv.(2)

open Printf

let () =
  let chan_in = open_in input_file in
  printf "let %s = [\n" variable_name;
  let buf = String.make 70 '\000' in
  let rec loop () =
    match input chan_in buf 0 70 with
    | 0 -> ()
    | more ->
      printf "%S;\n" (String.sub buf 0 more);
      loop ()
  in
  loop ();
  printf "]\n%!";
  close_in chan_in
