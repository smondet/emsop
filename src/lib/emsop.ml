
(** Emsop is and EDSL for Managing Some kinds of OCaml Projects 

  This is the top-level “pack” module

*)

open Nonstd
module Result = Pvem.Result
module String = struct
  include Sosa.Native_string
end
open Printf

open Result

open Docout

let global_debug_level = ref 2
let global_with_color = ref true
module Log = 
  Docout.Make_logger (struct
    type ('a, 'b) result = 'a
    let debug_level () = !global_debug_level
    let with_color () = !global_with_color
    let line_width = 72
    let indent = 4
    let print_string = Printf.eprintf "%s%!"
    let do_nothing () = ()
    let name = "emsop"
  end)

let failwithf fmt =
  ksprintf (fun str ->
    Log.(s "Failing: " % s str @ error);
    failwith str
  ) fmt


module In_channel = struct
  let input_all t =
    (* We use 65536 because that is the size of OCaml's IO buffers. *)
    let buf_size = 65536 in
    let buf = String.make buf_size '0' in
    let buffer = Buffer.create buf_size in
    let rec loop () =
      let len = input t buf 0 (String.length buf) in
      if len > 0 then begin
        Buffer.add_substring buffer buf 0 len;
        loop ();
      end
    in
    loop ();
    Buffer.contents buffer
end

module Feed = struct
  let kfeed f command data =
    let (ic, oc) as channels = Unix.open_process command in
    output_string oc data;
    close_out oc;
    let exn = ref None in
    (try
      while true do
        f (input_char ic)
      done
    with
      End_of_file -> ()
    | e -> exn := Some e);
    (match Unix.close_process channels with
       Unix.WEXITED 0 -> ()
     | _ -> invalid_arg ("feed_command: " ^ command));
    match !exn with
      Some e -> raise e
    | None -> ()

  let feed = kfeed print_char

  let ffeed oc command data = kfeed (output_char oc) command data

  let bfeed buf command data = kfeed (Buffer.add_char buf) command data

  let sfeed command data =
    let buf = Buffer.create (2 * String.length data) in
    bfeed buf command data;
    Buffer.contents buf
end

let commands_verbosity = Log.verbose

let cmdf fmt =
  ksprintf (fun str ->
      try
        let x = Sys.command str in
        Log.(s "CMD" % verbatim str % s "returns " % i x @ commands_verbosity);
      with
      | e ->
        Log.(s "CMD" % verbatim str % s "exn " % exn e @ commands_verbosity);
        raise e
    ) fmt

let cmdf_or_fail fmt =
  ksprintf (fun str ->
      let r = Sys.command str  in
      Log.(s "CMD" % verbatim str % s "returns " % i r @ commands_verbosity);
      if r = 0 then ()
      else failwithf "Command %S failed with status %d" str r
    ) fmt


let cmd_to_string ?feed fmt =
  ksprintf (fun cmd ->
      match feed with
      | Some s -> Feed.sfeed cmd s
      | None ->
        Unix.open_process_in cmd |> In_channel.input_all
    ) fmt

module File = struct

  let write file ~content =
    let o = open_out file in 
    SmartPrint.to_out_channel 72 4 o content;
    close_out o

  let write_string file ~content = write file (Metadoc.string content)

  let read file =
    try
      let i = open_in file in
      let content = In_channel.input_all i in
      close_in i;
      return content
    with e -> fail (`Exn e)
end

module OCaml = struct

  module Filename = struct

    let cma = sprintf "%s.cma"
    let cmi = sprintf "%s.cmi"
    let cmx = sprintf "%s.cmx"
    let cmo = sprintf "%s.cmo"
    let cmxa = sprintf "%s.cmxa"
    let cmxs = sprintf "%s.cmxs"
    let a = sprintf "%s.a"
    let o = sprintf "%s.o"
    let ml = sprintf "%s.ml"
    let mli = sprintf "%s.mli"
    let installable n ~with_native =
      List.map ~f:(fun f -> f n) 
        (if with_native
         then [cmx; cmo; cma; cmi; cmxa; cmxs; a; o]
         else [cmo; cma; cmi])

  end

  type code =
    | Raw of string
    | Concat of string * code list
    | Sequence of code list

  let rec to_string = function
  | Raw s -> s
  | Concat (sep, l) ->
    String.concat ~sep (List.map ~f:to_string l)
  | Sequence l ->
    String.concat ~sep:";\n" (List.map ~f:to_string l)

  let raw s = Raw s
  let rawf fmt = ksprintf raw fmt
  let lines l = Concat ("\n", l)
  let raw_lines l = lines (List.map l raw)
  let concat l ~sep = Concat (sep, l)

  let main c =
    lines [raw "let () = begin"; c; raw "end"]

  let sequence l = Sequence l

  let match_with expr ~patterns =
    rawf "match %s with\n%s" (to_string expr)
      (List.map patterns (fun (p, action) ->
           sprintf "| %s ->(\n%s\n)\n" p (to_string action))
       |> String.concat ~sep:"")

  let list_of_strings l =
    rawf "[\n%s\n]\n"
      (List.map l (sprintf "    %S;\n") |> String.concat ~sep:"")

  let module_name base =
    let open Option in
    (String.get base 0
     >>= fun c ->
     String.set base ~index:0 ~v:(Char.uppercase c))
    |> value ~default:base

end

module External = struct
  type t = {
    kind: [`Library | `Syntax];
    findlib_name: string;
    opam_name: string;
    version_constraints: (string option * string option);
  }
  let create ~findlib ?opam ?at_least ?at_most kind =
    let opam_name = Option.value opam ~default:findlib in
    let version_constraints = at_least, at_most in
    { kind; opam_name; findlib_name = findlib; version_constraints }

  let with_constraints ?at_least ?at_most ext =
    let version_constraints = at_least, at_most in
    { ext with version_constraints }

  let to_string ?(style=`Long) t =
    let version =
      match t.version_constraints with
      | None, None -> ""
      | Some s, None -> sprintf " (`≥ %s`)" s
      | None, Some s -> sprintf " (`≤ %s`)" s
      | Some s1, Some s2 when s1 = s2 -> sprintf " (`= %s`)" s1
      | Some s1, Some s2 ->  sprintf " (`∈ [%s, %s]`)" s1 s2 in
    match style with
    | `Long ->
      sprintf "%s `%S`%s (opam package: `%s`)"
        (match t.kind with
         | `Library -> "Library"
         | `Syntax -> "Syntax")
        t.findlib_name
        version
        t.opam_name
    | `Short -> sprintf "`%s`%s" t.findlib_name version

  let to_markdown t =
    let version =
      match t.version_constraints with
      | None, None -> ""
      | Some s, None -> sprintf " (`≥ %s`)" s
      | None, Some s -> sprintf " (`≤ %s`)" s
      | Some s1, Some s2 when s1 = s2 -> sprintf " (`= %s`)" s1
      | Some s1, Some s2 ->  sprintf " (`∈ [%s, %s]`)" s1 s2 in
    Markdown.(
      s (match t.kind with | `Library -> "Library" | `Syntax -> "Syntax")
      % s " `" % s t.findlib_name % s "`"
      % s version
      % s " (opam package: `" % s t.opam_name % s "`)")

end

module Repository = struct

  type git = {
    location: [`Github | `Bitbucket];
    user: string;
    name: string;
  }
  type t = [ `Git of git ]

  let git location ~user ~name : t = `Git {location; user; name}

  let to_string ?(style=`Long) t =
    match t with
    | `Git { location = `Github; user; name } ->
      sprintf "Github:%s/%s" user name
    | `Git { location = `Bitbucket; user; name } ->
      sprintf "Bitbucket:%s/%s" user name

  let git_host p =
    (match p.location with
     | `Github -> "github.com"
     | `Bitbucket -> "bitbucket.org")

  let home = function
  | `Git p ->
    sprintf "https://%s/%s/%s" (git_host p) p.user p.name

  let to_markdown = function
  | `Git {location; user; name} as t ->
    Markdown.(link (sf "%s/%s" user name) ~url:(home t))

  let ssh = function
  | `Git p ->
    sprintf "git@%s:%s/%s" (git_host p) p.user p.name

  let tar_gz ?(reference="master") = function
  | `Git p ->
    begin match p.location with
    | `Github ->
      sprintf "https://%s/%s/%s/archive/%s.tar.gz"
        (git_host p) p.user p.name reference
    | `Bitbucket ->
      sprintf "https://%s/%s/%s/get/%s.tar.gz"
        (git_host p) p.user p.name reference
    end

  let git_repo_is_clean path =
    ksprintf (fun s -> Sys.command s = 0)
      "cd %s && test `git status --porcelain | grep -v '??' | wc -l` -eq 0"
      path

  let git_tag_exists path tag =
    ksprintf (fun s -> Sys.command s = 0)
      "cd %s && test `git tag | grep '%s' | wc -l` -ge 1" path tag
end


module Person = struct
  type t = {print: string option; email: string; webpage: string option;}
  let create ?print ?webpage email = {print; email; webpage}
  let to_markdown p =
    let name = Option.value p.print ~default:(sprintf "`%s`" p.email) in
    match p.webpage with
    | Some w -> sprintf "[%s](%s)" name w |> Markdown.s
    |  None -> name |> Markdown.s

  let contact p =
    match p.print with
    | Some pr -> sprintf "%s <%s>" pr p.email
    | None -> p.email
end

module License = struct

  type t = [`ISC]

  let name = function
  | `ISC -> "ISC"

end

module Package = struct

  type t = {
    name: string;
    description: string;
    long_description: string option;
    developed_version: string; 
    parent_module: string; (* the pack-like module *)
    (* current_version: the one in development, or about to be released *)
    releases: string list;
    repository: Repository.t option;
    dependencies: External.t list;
    homepage: string option;
    maintainer: Person.t option;
    authors: Person.t list;
    build_system: [ `Please of [`Atdgen of string ] list | `Omake ];
    since: int;
    license: License.t;
    kind: [`Library | `Executable ];
  }
  let create 
      ?repository
      ?(dependencies=[]) ?(description="NO DESCRIPTION")
      ?homepage ?since ?maintainer ?(releases=[]) ?long_description
      ?(build_system=`Please []) ?parent_module_name
      ?(developed_version="0") ?(license=`ISC) ?(authors=[])
      ?(kind=`Library) name =
    let since = Option.value since ~default:2013 in
    let parent_module =
      Option.value parent_module_name ~default:(OCaml.module_name name) in
    let repository =
      Option.(repository >>= function
        | `In make -> Some (make name)
        | `Custom repo -> Some repo) in
    {name; description; developed_version; repository; long_description;
     build_system; dependencies; releases; homepage; maintainer; authors; since;
     license; kind; parent_module}

  let version_string t ~dev =
    sprintf "%s%s" t.developed_version (if dev then "-dev" else "")

  let to_markdown t =
    let open Markdown in
    let list_or_empty ?(empty=empty) ~f = function [] -> empty | l -> f l in
    let plural_of_list (si,pl) = function [one] -> s si | _ -> s pl in
    let sublist_for_more ~f = 
      function [one] -> s " " % f one
             | more -> n % (ul_inner (List.map ~f more)) in
    let cut_paragraphs str =
      let lines = String.split ~on:(`Character '\n') str in
      List.map lines ~f:(function "" -> new_par | nonempty -> s nonempty)
      |> concat in
    h2 (s (match t.kind with `Library -> "Library" | `Executable -> "Executable")
      % space % s t.name)
    % emph (s t.description)
    % new_par
    % option (fun ld -> cut_paragraphs ld % new_par) t.long_description 
    % ul_filter [
        option (fun r -> s "Homepage: " % url r) t.homepage;
        list_or_empty t.releases ~f:(fun some ->
          s "Available versions: " 
          % separate (s ", ") (List.map ~f:s some));
        s "Currently developing version: " % s t.developed_version;
        option (fun r -> s "Repository: "  % Repository.to_markdown r)
          t.repository;
        list_or_empty  t.dependencies ~empty:(s "No dependencies") 
          ~f:(fun some ->
            s "Dependenc" % plural_of_list ("y: ", "ies: ") some
            % sublist_for_more ~f:External.to_markdown some);
        option (fun p -> s "Maintainer: " % Person.to_markdown p) t.maintainer;
        list_or_empty t.authors ~f:(fun authors ->
          s "Author" % plural_of_list (":", "s:") authors
          % sublist_for_more ~f:Person.to_markdown authors);
        s "License: " % s (License.name t.license)
        % s ", since " % i t.since;
      ]

  let standard_version_tag ?for_version t =
    sprintf "%s.%s" t.name (Option.value for_version 
                              ~default:(version_string t ~dev:true))

  let get t ~from_path =
    match t.repository with
    | Some r ->
      cmdf_or_fail "cd %s && git clone %s" from_path (Repository.ssh r);
      return ()
    | None -> fail (`repository_not_specified)

end

module Build = struct

  open Package

  let build_command t =
    begin match t.build_system with
    | `Please _ -> ["ocaml"; "please.ml"; "build"];
    | `Omake -> ["omake"]
    end

  let stall_command what t =
    begin match t.build_system with
    | `Please _ -> 
      let options =
        match t.kind with
        | `Library -> []
        | `Executable -> ["--prefix"; "%{prefix}%"] in
      ["ocaml"; "please.ml"; what] @ options
    | `Omake ->
      let options =
        match t.kind with
        | `Library -> []
        | `Executable -> ["PREFIX=%{prefix}%"] in
      ["omake"; what] @ options
    end
  let install_command t = stall_command "install" t
  let uninstall_command t = stall_command "uninstall" t

  let build_dependencies t =
    if t.build_system = `Omake then ["\"omake\""] else []

  module Script = struct

    let _header script_name =
        "#! /usr/bin/env ocaml\n\
          \ open Printf\n\
          \ module List = ListLabels\n\
          \ let say fmt = ksprintf (printf \"Please-> %s\\n%!\") fmt\n\
          \ let cmdf fmt =\n\
          \   ksprintf (fun s -> ignore (Sys.command s)) fmt\n\
          \ let chain l =\n\
          \   List.iter l ~f:(fun cmd ->\n\
          \       printf \"! %s\\n%!\" cmd;\n\
          \       if  Sys.command cmd = 0\n\
          \       then ()\n\
          \       else ksprintf failwith \"%S failed.\" cmd\n\
          \     )\n\
          \ let args = Array.to_list Sys.argv\n\
          \ let in_build_directory f =\n\
          \   cmdf \"mkdir -p _build/\";\n\
          \   Sys.chdir \"_build/\";\n\
          \   begin try\n\
          \     f ();\n\
          \   with\n\
          \     e ->\n\
          \     Sys.chdir \"../\";\n\
          \     raise e\n\
          \   end;\n\
          \   Sys.chdir \"../\";\n\
          \   ()\n\
        "

  let ocamlfind_options t =
    let has_syntax = ref false in
    let has_threads = ref false in
    let packs =
      List.map t.dependencies (fun ext ->
          if ext.External.kind = `Syntax then has_syntax := true;
          if ext.External.findlib_name = "threads" then has_threads := true;
          ext.External.findlib_name) in
    match packs with
    | [] -> ""
    | l ->
      sprintf "-package %s%s%s" (String.concat ~sep:"," l)
        (if !has_syntax then " -syntax camlp4o" else "")
        (if !has_threads then " -thread" else "")

  let _one_file_build_chain t =
    let open OCaml.Filename in
    let plugin_commands, plugin_files = 
      match t.build_system with
      | `Omake -> ([], [])
      | `Please plugs ->
        List.map plugs (function
          | `Atdgen f ->
            ([
                sprintf  "cp ../%s.atd ." f;
                sprintf "atdgen -t %s.atd" f;
                sprintf "atdgen -j -j-std %s.atd" f;
                sprintf "rm -f %s_t.mli" f;
                sprintf "rm -f %s_j.mli" f;
            ], 
             [(*sprintf "%s_t.mli" f;*) sprintf "%s_t" f;
              (*sprintf "%s_j.mli" f;*) sprintf "%s_j" f]))
        |> List.split
        |> (fun (one, two) -> List.concat one, List.concat two)
    in
    let build_commands =
      let basenames = plugin_files @ [ t.name ] in
      let cmos = List.map basenames cmo |> String.concat ~sep:" " in
      let cmxes = List.map basenames cmx |> String.concat ~sep:" "  in
      let ocamlcs =
        List.map basenames
          ~f:(fun name -> 
              sprintf "ocamlfind ocamlc %s -c %s -o %s"
                (ocamlfind_options t) (ml name) (cmo name)) in
      let ocamlopts =
        List.map basenames
          ~f:(fun name -> 
              sprintf "ocamlfind ocamlopt %s -c %s  -annot -bin-annot -o %s"
                (ocamlfind_options t) (ml name) (cmx name)) in
      match t.kind with
      | `Library ->
        [ sprintf "cp ../%s ." (ml t.name); ]
        @ plugin_commands
        @ ocamlcs
        @ ocamlopts
        @ [
          sprintf "ocamlc %s -a -o %s" cmos (cma t.name);
          sprintf "ocamlopt %s -a -o %s" cmxes (cmxa t.name);
          sprintf "ocamlopt %s %s -shared -o %s"
            (cmxa t.name) (a t.name) (cmxs t.name);
        ]
      | `Executable ->
        [ sprintf "cp ../%s ." (ml t.name); ]
        @ plugin_commands
        @ ocamlcs
        @ ocamlopts
        @ [
          sprintf "ocamlfind ocamlc %s %s -linkpkg -o %s.byte"
            (ocamlfind_options t) cmos t.name;
          sprintf "ocamlfind ocamlopt %s %s -linkpkg -o %s"
            (ocamlfind_options t) cmxes t.name;
        ]
    in 
    OCaml.(lines [ 
        raw "let build () =";
        raw "in_build_directory (fun () ->";
        raw "chain ";
        OCaml.list_of_strings build_commands;
        raw ")";
      ] |> to_string)

  let _install_function t =
    match t.kind with
    | `Library ->
      sprintf
        "let install () =\n\
          \    in_build_directory (fun () ->\n\
          \        chain [\n\
          \          \"ocamlfind install %s ../META %s\"\n\
        \        ])\n"
        t.name (String.concat ~sep:" " 
          (OCaml.Filename.installable ~with_native:true t.name))
    | `Executable ->
      sprintf
        "let install prefix =\n\
          \   chain [\n\
          \     sprintf \"mkdir -p %%s/bin/\" prefix;\n\
          \     sprintf \"cp _build/%s %%s/bin/\" prefix;\n\
        \   ]\n"
        t.name
  let _uninstall_function t =
    match t.kind with
    | `Library ->
      sprintf
        "let uninstall () =\n\
          \    chain [\n\
          \      \"ocamlfind remove %s\"\n\
        \    ]\n"
        t.name
    | `Executable ->
      sprintf
        "let uninstall prefix =\n\
          \    chain [\n\
          \      sprintf \"rm -f %%s/bin/%s\" prefix\n\
        \    ]\n"
        t.name

  let _merlinize_function t =
    sprintf
      "let merlinize () =\n\
        \    chain [\n\
        \      \"echo 'S .' > .merlin\";\n\
        \      \"echo 'B _build' >> .merlin\";\n\
        \      %s\n\
        \     ]\n\
      "
      (List.map t.dependencies (function d ->
         sprintf "\"echo 'PKG %s' >> .merlin\";" d.External.findlib_name)
       |> String.concat ~sep:"\n")

  let _build_doc_function t =
    sprintf  "let build_doc () =\n\
               \    in_build_directory (fun () ->\n\
               \        chain [\n\
               \          \"mkdir -p doc\";
               \          sprintf \"ocamlfind ocamldoc %s -charset UTF-8 \
               -keep-code -colorize-code -html %s.ml -d doc/\";\n\
               \        ])\n\
             "
      (ocamlfind_options t) t.name

  let _script_main t =
    let open OCaml in
    let patterns =
      let verbose_action ~msg action =
        sequence [
          rawf "say %S" msg;
          action;
          rawf "say \"Done.\"";
        ] in
      let if_library pattern =
        if t.kind = `Library then Some pattern else None in
      let cmd_pattern ?(tail="[]") command =
        sprintf "_ :: %S :: %s" command tail in
      let tail_with_prefix_if_exec =
        if t.kind = `Executable then "[\"--prefix\"; prefix]" else "[]" in
      let call_with_prefix_if_exec =
        if t.kind = `Executable then rawf "%s prefix" else rawf "%s ()" in
      List.filter_map ~f:(fun e -> e) [
        Some (cmd_pattern "build",
              verbose_action ~msg:"Building" (raw "build ()")); 
        if_library (
          cmd_pattern "build_doc",
          verbose_action ~msg:"Building Documentation"
            (raw "build_doc ()"));
        Some (cmd_pattern "install" ~tail:tail_with_prefix_if_exec,
              verbose_action ~msg:"Installing" 
                (call_with_prefix_if_exec "install"));
        Some (cmd_pattern "uninstall" ~tail:tail_with_prefix_if_exec,
              verbose_action ~msg:"Uninstalling" 
                (call_with_prefix_if_exec "uninstall"));
        Some (cmd_pattern "merlinize",
              verbose_action ~msg:"Updating `.merlin` file" 
                (raw "merlinize ()")); 
        Some (cmd_pattern "clean",
              verbose_action ~msg:"Cleaning"
                (rawf "cmdf %S" "rm -fr _build"));
        Some ("_",
              rawf "say %S Sys.argv.(0)"
                (sprintf
                   "usage: ocaml %%s \
                    [build|install|uninstall|clean|%smelinize]%s"
                   (if t.kind = `Library then "build_doc|" else "")
                   (if t.kind = `Executable 
                    then "\n(for `(un)install` arguments `--prefix \
                          [PREFIX_PATH]` are mandatory)."
                    else "")
                ));
      ]
    in
    main (match_with (raw "args") ~patterns)
    |> to_string

  let path ~from_path = Filename.concat from_path "please.ml"

  let basic_build_script t =
    (String.concat ~sep:"\n\n" [
       (_header ());
       (_one_file_build_chain t);
       (_install_function t);
       (_uninstall_function t);
       (_merlinize_function t);
       (_build_doc_function t);
       sprintf "let name = %S" t.name;
       (_script_main t);
       "\n"
     ])

  end

end

module META_file = struct

  let path ~from_path = Filename.concat from_path "META"

  let content t ~dev =
    let open Package in
    let open OCaml.Filename in
    let open Metadoc in
    let requires =
      List.filter_map t.dependencies (fun ext ->
          let open External in
          if ext.kind = `Library then Some ext.findlib_name else None)
    in
    let meta_field name value =
      s name % s " = " % sf "%S" value % n in
    meta_field "version" (version_string t ~dev)
    % meta_field "description" t.description
    % meta_field "archive(byte)" (cma t.name)
    % meta_field "archive(byte,plugin)" (cma t.name)
    % meta_field "archive(native)" (cmxa t.name)
    % meta_field "archive(native,plugin)" (cmxs t.name)
    % meta_field "exists_if" (cma t.name)
    % (match requires with
    | [] -> empty
    | some -> meta_field "requires" (String.concat ~sep:" " requires))

end

module Opam = struct

  let package_path t ~repo ~version =
    sprintf "%s/packages/%s/%s.%s/" repo t.Package.name t.Package.name version

  let package_file ~file t ~repo ~version =
    Filename.concat (package_path t ~repo ~version) file

  let opam_path  = package_file ~file:"opam"
  let descr_path = package_file ~file:"descr"
  let url_path   = package_file ~file:"url"

  let dependencies t =
    let to_opam ext =
      let open External in
      sprintf "%S%s" ext.opam_name
        (match ext.version_constraints with
        | None, None -> ""
        | Some s, None -> sprintf " {>= %S }" s
        | None, Some s -> sprintf " {<= %S }" s
        | Some s1, Some s2 when s1 = s2 -> sprintf " { = %S }" s1
        | Some s1, Some s2 -> sprintf " { >= %S & <= %S }" s1 s2) in
    sprintf "%S" "ocamlfind" :: Build.build_dependencies t
    @ (List.map t.Package.dependencies ~f:to_opam)
  
  let opam_file_content t =
    let open Metadoc in
    let open Package in
    let opam_field name f = s name % s ": " % f % n in
    let quote str = sprintf "%S" str |> s in
    let opam_string_field name str = opam_field name (quote str) in
    let list_of_commands cmds =
      indent (brakets (
          n %
          (List.map cmds ~f:(fun cmd ->
             brakets (List.map cmd ~f:quote |> separate space) % n)
           |> separate space))) in
    opam_string_field "opam-version" "1"
    % option t.maintainer
        ~f:(fun m -> opam_string_field "maintainer" m.Person.email)
    % Option.(
        match t.homepage, t.repository with
        | Some h, _ -> opam_string_field "homepage" h
        | None, Some repo  -> 
          Repository.home repo |> opam_string_field "homepage"
        | _ -> empty)
    % opam_field "ocaml-version" (s "[>= \"4.00.0\"]")
    % opam_field "build" (list_of_commands [
          Build.build_command t;
          Build.install_command t;
        ])
    % opam_field "remove" (list_of_commands [
          Build.uninstall_command t;
        ])
    % opam_field "depends" 
        (brakets (List.map ~f:string (dependencies t) |> separate space))

  let opam_descr_content t = 
    Metadoc.(s t.Package.description % n
      % Option.(
          (t.Package.long_description >>= fun st -> Some (n % s st % n))
          |> value ~default:empty))

  let opam_url_content ?(with_checksum=false) t ~kind ~version =
    let open Metadoc in
    begin match kind, t.Package.repository with
    | `dev, Some r -> sf "git: %S\n" (Repository.ssh r) % n
    | `version version, Some r ->
      let reference = Package.standard_version_tag t ~for_version:version in
      let tar_gz_url = Repository.tar_gz r ~reference in
      let tar_gz = Filename.basename tar_gz_url in
      let checksum_part =
        if with_checksum then (
          Log.(s "Getting Checksum." @ verbose);
          let checksum =
            begin try
              cmdf_or_fail "wget %S -O /tmp/%s" tar_gz_url tar_gz;
              cmd_to_string "md5sum /tmp/%s" tar_gz 
              |> String.split ~on:(`Character ' ')
              |> List.filter ~f:((<>) "")
              |> List.hd
            with e -> None
            end
          in
          begin match checksum with
          | None ->
            Log.(s "No checksum for " % s tar_gz_url @ warning);
            empty
          | Some c ->
            Log.(s "Checksum for " % s tar_gz % s " is " % s c @ verbose);
            sf "checksum: %S" c % n
          end
        ) else empty in
      sf "archive: %S\n" (Repository.tar_gz r ~reference) % n % checksum_part
    | _, None ->
      Log.(sf "repository not specified for %s" t.Package.name
             @ warning);
      empty
    end


  let update ~with_checksum ~packages ~path =
    let repo = path in
    List.iter packages (fun package ->
      let todo =
        (`dev, Package.version_string package ~dev:true)
        :: (let v = Package.version_string package ~dev:false in 
          (`version v, v))
        :: List.map package.Package.releases ~f:(fun v -> (`version v, v))
      in
      List.iter todo (fun (kind, version) ->
        cmdf_or_fail "mkdir -p %s" (package_path package ~repo ~version);
        File.write (opam_path package ~repo ~version)
          ~content:(opam_file_content  package);
        File.write (url_path package ~repo ~version) 
          ~content:(opam_url_content ~with_checksum  package ~kind ~version);
        File.write (descr_path package ~repo ~version) 
          ~content:(opam_descr_content  package);
      );
    )

end

module LICENSE_file = struct

  let path ~from_path = Filename.concat from_path "LICENSE"

  let content t =
    let open Package in
    let open Metadoc in
    sf "Copyright (c) %d," t.since
    % (match t.authors with
    | [] ->
      Option.((map t.maintainer ~f:(fun p -> space % s (Person.contact p)))
              |> value ~default:(s "NOBODY"))
    | [one] ->  space % s (Person.contact one)
    | more ->
      n % indent (
        separate n (List.map more (fun p -> sf "- %s" (Person.contact p)))
      ))
    % n % n
    % concat (List.map Emsop_license.isc ~f:s) % n

end

module INSTALL_file = struct

  let path ~from_path = Filename.concat from_path "INSTALL"

  let content t =
    let open Package in
    let open Markdown in
    let code_block tl = indent (separate n tl) %n %n  in
    h1 (s "BUILD INSTRUCTIONS")
    % h2 (s "Build")
    % (match t.dependencies  with
    | [] ->
      par (s "This package does not have dependencies, just do:")
    | some ->
      par (s  "You need the following libraries:")
      % ul_filter (List.map some ~f:(fun ext ->  External.to_markdown ext))
      % par (s "Then you can build the library:"))
    % code_block [Build.build_command t |> String.concat ~sep:" " |> s]
    % h2 (s "Install")
    % (match t.kind with
    | `Library ->
      par (s "You can install the library with `ocamlfind`:")
      % code_block [Build.install_command t |> String.concat ~sep:" " |> s]
      % par (s "You may want to control the destination with the variable \
                `OCAMLFIND_DESTDIR`.")
    | `Executable ->
      par (s "You can install the application with a given “prefix”:")
      % code_block [Build.install_command t |> String.concat ~sep:" " |> s]
      % par (s "The executable will be copied to `$PREFIX/bin/`."))
    % h2 (s "Uninstall")
    % (match t.kind with
    | `Library ->
      par (s "You can uninstall the library (also `ocamlfind`):")
      % code_block [Build.uninstall_command t |> String.concat ~sep:" " |> s]
    | `Executable ->
      par (s "You can uninstall the application from the “prefix”:")
      % code_block [Build.uninstall_command t |> String.concat ~sep:" " |> s]
      % par (s "The executable will be removed from `$PREFIX/bin/`."))
    % n

end

module Update = struct

  let needs_META t =
    match t.Package.build_system with
    | `Omake -> false
    | `Please _ -> t.Package.kind = `Library

  let needs_please_dot_ml t =
    match t.Package.build_system with
    | `Omake -> false
    | `Please _ -> true

  let run packages ~run_type ~from_path ~release =
    let open Package in
    let potentially_write file ~content =
      let ret log modifications = 
        object method log = log method modified = modifications end in
      let write () =
        match run_type with
        | `Dry -> 
          ret Log.(s "Would be different (dry-run).") []
        | `Apply ->
          File.write file ~content;
          ret Log.(s "Updated.") [file]
      in
      begin match File.read file with
      | `Ok current ->
        let as_string = Metadoc.to_string 72 4 content in
        if current = as_string then
          ret Log.(s " Nothing to change.") []
        else 
          write ()
      | `Error (`Exn e) ->
        Log.(s "Reading " % s file % s " Exn: " % exn e @ very_verbose);
        let log = write () in
        ret Log.(s " Can't read original one, "  % log#log) log#modified
      end
    in
    List.fold ~init:(return ()) packages ~f:(fun prev package ->
      prev >>= fun () ->
      let sentence = ref Log.(s "Package " % s package.Package.name % n) in
      let from_path = Filename.concat from_path package.name in
      let modifications = ref [] in
      let add_modifications r =
        modifications := !modifications @ r#modified in
      if needs_META package then (
        let path = META_file.path ~from_path in
        Log.(sentence := !sentence % s "- META " % parens (s path));
        let content = META_file.content package ~dev:(not release) in
        let ret = potentially_write path ~content in
        add_modifications ret;
        Log.(sentence := !sentence % ret#log %n)
      );
      if needs_please_dot_ml package then (
        let path = Build.Script.path ~from_path in
        Log.(sentence := !sentence % s "- Build Script: " % parens (s path));
        let content =
          Metadoc.string (Build.Script.basic_build_script package) in
        let ret = potentially_write path ~content in
        add_modifications ret;
        Log.(sentence := !sentence % ret#log %n)
      );
      begin (* INSTALL *)
        let path = INSTALL_file.path ~from_path in
        Log.(sentence := !sentence % s "- INSTALL: " % parens (s path));
        let content = INSTALL_file.content package in
        let ret = potentially_write path ~content in
        add_modifications ret;
        Log.(sentence := !sentence % ret#log %n)
      end;
      begin (* LICENSE *)
        let path = LICENSE_file.path ~from_path in
        Log.(sentence := !sentence % s "- LICENSE: " % parens (s path));
        let content = LICENSE_file.content package in
        let ret = potentially_write path ~content in
        add_modifications ret;
        Log.(sentence := !sentence % ret#log %n)
      end;
      begin match !modifications with
      | [] -> Log.(sentence := !sentence % s "⇒ No modifications" %n)
      | more ->
        Log.(sentence := !sentence % s "⇒ Modifications: " % 
              OCaml.list s more % n);
      end;
      Log.(!sentence @ normal);
      return ())

end

module About = Emsop_about

module Define = struct

  let depends_on = External.create
  let person = Person.create
  let package = Package.create
  let library = Package.create ~kind:`Library
  let executable = Package.create ~kind:`Executable

  let github ~user ~name = Repository.git `Github ~user ~name
  let bitbucket ~user ~name = Repository.git `Bitbucket ~user ~name

end

module Command_line = struct

  let cmdliner_main ~packages ~additional_terms ~default_from_path =
    let open Cmdliner in

    let version = About.version in
    let packages_option =
      let doc = "Restrict the package list $(docv) (comma separated)." in
      Term.(
        pure (fun package_names ->
          match package_names with
          | [] -> packages
          | some -> 
            List.filter packages (fun p -> 
              List.exists some ~f:(fun n -> p.Package.name = n)))
        $ Arg.(value & opt (list string) []
               & info ["P"; "packages"] ~docv:"PACKS" ~doc)
      )
    in
    let from_path_option =
      Term.(Arg.(value & opt string default_from_path
                 & info ["F"; "from"] ~docv:"PATH"
                   ~doc:"Use $(docv) as parent path.")) in
    let release_option =
      Term.(Arg.(value & flag & info ["R"; "release"] 
                   ~doc:"Consider developed version as a “release” version (i.e.
                        don't append `-dev`).")) in
    let wet_run_option action_word =
      Term.(Arg.(value & flag & info ["W"; "wet-run"] 
                   ~doc:(sprintf "Actually do the %s (i.e. not “dry-run”)."
                       action_word))) in
    let display_cmd =
      let doc = "Display packages in this instance." in
      let man = [] in
      Term.(
        pure (fun packages ->
          let text = 
            Markdown.(
              begin
                h1 (s "Packages")
                % separate new_par (List.map ~f:Package.to_markdown packages)
              end
              |> to_markdown_string
            ) in
          print_string text;
          return ())
        $ packages_option
      ),
      Term.info "display" ~version ~sdocs:"COMMON OPTIONS" ~doc ~man
    in
    let opam_cmd =
      Term.(
        pure (fun packages with_checksum path ->
          Log.(s "opam paths: " % s path @ verbose);
          Opam.update ~packages ~path ~with_checksum;
          return ())
        $ packages_option
        $ Arg.(value & flag & info ["C"; "checksums"] 
                 ~doc:(sprintf "Also compute md5sums for the `url` files."))
        $ Arg.(required & pos 0 (some string) None
                 (info [] ~docv:"PATH" ~doc:"Path to the opam repo"))
      ),
      Term.info "opam" ~version
        ~sdocs:"COMMON OPTIONS" ~doc:"update opam packages"
    in
    let fetch_cmd =
      Term.(
        pure (fun packages from_path ->
        List.fold ~init:(return ()) packages ~f:(fun prev package ->
          prev >>= fun () ->
          let sentence = Log.(s "Package " % s package.Package.name) in
          let readme =
            Filename.(concat from_path
                (concat package.Package.name "README.md")) in
          if Sys.file_exists readme 
          then (
            Log.(sentence % s ": already exists " % parens (s readme) @ normal);
            return ()
          ) else (
            match Package.get package ~from_path with
            | `Ok () -> 
              Log.(sentence % s ": Done." @ normal);
              return ()
            | `Error `repository_not_specified ->
              Log.(sentence % s ": No repository." @ error);
              return ()
          )))
        $ packages_option
        $ from_path_option),
      Term.info "fetch" ~version ~sdocs:"COMMAND OPTIONS"
        ~doc:"fetch the code"
    in
    let file_cmd =
      Term.(
        pure (fun file package_name ->
          let packopt =
            List.find packages (fun p -> p.Package.name = package_name) in
          let line_width = 72 in
          let indent = 4 in
          match file, packopt with
          | _, None ->
            Log.(s "Unknown package: " % sf "%S" package_name @ error);
            fail ()
          | "please.ml", Some package ->
            print_string (Build.Script.basic_build_script package);
            return ()
          | "META", Some package ->
            print_string (META_file.content package ~dev:true
                          |> Metadoc.to_string ~line_width ~indent);
            return ()
          | "LICENSE", Some package ->
            print_string (LICENSE_file.content package
                          |> Metadoc.to_string ~line_width ~indent);
            return ()
          | "INSTALL", Some package
          | "INSTALL.md", Some package ->
            print_string (INSTALL_file.content package
                          |> Metadoc.to_string ~line_width ~indent);
            return ()
          | other, Some _ ->
            Log.(s "Unknown file: " % sf "%S" other @ error);
            fail ())
        $ Arg.(required & pos 0 (some string) None
                 (info [] ~docv:"FILE"
                    ~doc:"File to gsenerate (META, please.ml, …)"))
        $ Arg.(required & pos 1 (some string) None
                 (info [] ~docv:"PACKAGE" ~doc:"Package concerned."))
      ),
      Term.info "generate-file" ~version ~sdocs:"COMMON OPTIONS"
        ~doc:"Display contents of a generated file."
    in
    let update_cmd =
      let open Term in
      let cmd_info = info "update" ~version ~sdocs:"COMMON OPTIONS"
          ~doc:"Update generated files in checked-out repositories" in
      let cmd_term =
        pure (fun packages from_path release wet_run ->
          let run_type = if wet_run then `Apply else `Dry in
          Update.run packages ~from_path ~release ~run_type
        )
        $ packages_option
        $ from_path_option
        $ release_option
        $ wet_run_option "update"
      in
      cmd_term, cmd_info in
    let cmds =
      additional_terms @ 
      [display_cmd; opam_cmd; fetch_cmd; update_cmd; file_cmd] in
    match Term.eval_choice display_cmd cmds with
    | `Ok f -> f
    | `Error _ -> exit 1
    | _ -> exit 0

  let run_main ~packages ~additional_terms ~default_from_path =
    match (cmdliner_main ~packages ~additional_terms ~default_from_path) with
    | `Ok () -> exit 0
    | `Error () -> exit 1

end

