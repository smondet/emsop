
open Nonstd
module String = Sosa.Native_string
module Result = Pvem.Result
open Printf


let packages = 
  let open Emsop.Define in
  let seb =
    person "seb@mondet.org"
      ~print:"Sebastien Mondet"
      ~webpage:"http://seb.mondet.org" in
  let seb2 =
    person "seb2@mondet.org"
      ~print:"Sebastien Mondet II"
  in
  let seb3 = person "seb3@mondet.org" in
  let my_bitbucket name = bitbucket ~name ~user:"smondet" in
  let depends_on_nonstd = depends_on `Library ~findlib:"nonstd" in
  let depends_on_sosa =  depends_on `Library ~findlib:"sosa" in
  let depends_on_pvem =  depends_on `Library ~findlib:"pvem" in
  let depends_on_cmdliner =  depends_on `Library ~findlib:"cmdliner" in
  let depends_on_omd =  depends_on `Library ~findlib:"omd" in
  let depends_on_smart_print =
    depends_on `Library ~findlib:"smart_print" ~opam:"smart-print" in
  [
    package "emsop"
      ~developed_version:"0.0.0"
      ~description:"EDSL For Managing Some OCaml Projects"
      ~long_description:"With EMSop one can define “projects” concisely and \
                         do stuff like generating `opam` packages and so on"
      ~maintainer:seb
      ~repository:(`In my_bitbucket)
      ~build_system:`Omake
      ~dependencies:[
        depends_on_nonstd;
        depends_on_sosa;
        depends_on_pvem;
        depends_on_cmdliner;
        depends_on_omd;
        depends_on_smart_print;
      ];
    package "all_default";
    library "a_bit_more"
      ~developed_version:"0.1.0"
      ~description:"Description of “a bit more”"
      ~long_description:"This is the long description of “a bit more”, it \
                          spans multiple lines of meaningless text.\n\n\
                         With more than one paragraph."
      ~homepage:"http://seb.mondet.org"
      ~releases:["0.0.1"; "0.0.2"]
      ~maintainer:seb
      ~authors:[seb2; seb3]
      ~repository:(`In my_bitbucket)
      ~dependencies:[
        depends_on `Library
          ~findlib:"smart_print"
          ~opam:"smart-print";
        depends_on `Library ~findlib:"omd" ~at_least:"0.8.0";
      ];
    executable "another_one"
      ~description:"Package with *one* author and *one* dependency"
      ~authors:[seb]
      ~repository:(`Custom (github ~name:"aaaannnnotherone" ~user:"uuuser"))
      ~dependencies:[
        depends_on `Library ~findlib:"smart_print" ~opam:"smart-print";
      ];
]

let cmdf fmt = ksprintf (fun s ->
    eprintf "cmd: %S\n%!" s;
    ignore (Sys.command s)) fmt

let () =
  let setup_test_cmd =
    let open Cmdliner in
    let open Term in
    let term =
      pure (fun dir ->
        cmdf "mkdir -p %s/opam/" dir;
        cmdf "mkdir -p %s/code/" dir;
        List.iter packages (fun p ->
          cmdf "mkdir -p %s/code/%s" dir p.Emsop.Package.name;
        );
        `Ok ()
      )
      $ Arg.(required & pos 0 (some string) None
               (info [] ~docv:"PATH" ~doc:"Path to do the test setup"))
    in
    let info = info "test-setup" ~version:"0"
        ~sdocs:"COMMON OPTIONS" ~doc:"setup the test directory" in
    term, info
  in
  Emsop.Command_line.run_main ~packages ~additional_terms:[setup_test_cmd]
    ~default_from_path:"/does_not_exist"
